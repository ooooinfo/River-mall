package org.linlinjava.litemall.wx.vo.wechart.returns;

import lombok.Data;

@Data
public class Code2SessionReturn {
	private String openid;
	private String session_key;
	private String unionid;
}
