package org.linlinjava.litemall.wx.vo.wechart;

import java.util.Map;

import lombok.Data;

@Data
public class TemplateData {
	/* private String keyword1;//取餐码
	 private String keyword2;//取餐门店
	 private String keyword3;//商户地址
	 private String keyword4;//下单时间
	 private String keyword5;//温馨提示
	 */
    private String value;
    public TemplateData(String value) {
    	this.value = value;
    }
    public TemplateData() {
    	
    }
}
