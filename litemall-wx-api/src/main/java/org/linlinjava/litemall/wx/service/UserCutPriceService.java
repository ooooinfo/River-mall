package org.linlinjava.litemall.wx.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.RedisUntil;
import org.linlinjava.litemall.db.domain.LitemallRedPacakets;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsHistory;
import org.linlinjava.litemall.db.domain.ReceiveRecord;
import org.linlinjava.litemall.db.domain.SysWxConfig;
import org.linlinjava.litemall.db.po.MngUserCutpricePo;
import org.linlinjava.litemall.db.service.LitemallRedPacaketsHistoryService;
import org.linlinjava.litemall.db.service.LitemallRedPacaketsService;
import org.linlinjava.litemall.db.service.ReceiveRecordService;
import org.linlinjava.litemall.db.service.SystemWxConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserCutPriceService {
    private final Log log = LogFactory.getLog(UserCutPriceService.class);
@Autowired
private LitemallRedPacaketsHistoryService litemallRedPacaketsHistoryService;
@Autowired
private LitemallRedPacaketsService litemallRedPacaketsService;
@Autowired
private SystemWxConfigService systemWxConfigService;
@Autowired
private WxGiveRedPackService wxGiveRedPackService;
@Autowired
private RedisUntil redisUntil;
@Autowired
private ReceiveRecordService receiveRecordService;

    public int enterprisePayment(LitemallRedPacaketsHistory sph, String subAppId) throws Exception {
        int flag = 0;
        //SysPlatformCutpriceHistory sph = platformCutpriceHistoryDao.findOne(id);
        List<LitemallRedPacaketsHistory> sps = litemallRedPacaketsHistoryService.findAuctionByUserIdAndStatus(sph.getUserId(), sph.getCutpriceId(), 0);
        LitemallRedPacakets platformCutprices = litemallRedPacaketsService.findById(sph.getCutpriceId());

        SysWxConfig wxConfig = systemWxConfigService.getconfigByAppletNam(subAppId);
        if (sph.getCutpriceMode() == 3 && sph.getStatus() == 1 && (sps == null || sps.size() <= 0)) {
            flag = wxGiveRedPackService.enterprisePayment(sph, wxConfig);

            if (flag == 1) {
                log.info("flag == 1");
                MngUserCutpricePo userCutprice = (MngUserCutpricePo) redisUntil.get(
                        "Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId());
               // log.info("userCutprice" + userCutprice);
                if (userCutprice != null) {
                    log.info("Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId() + "redis已重置");
                    userCutprice.setStatus(0);
                    redisUntil.setSeconds("Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId(), userCutprice, 24 * 60 * 60);
                }
                sph.setStatus(0);
                sph.setGainTime(LocalDateTime.now());
                log.info("用户昵称为:{"+userCutprice.getNickName()+"}领取了 {"+userCutprice.getPrice()/100+"/元}现金红包");
                litemallRedPacaketsHistoryService.updateStatus(sph);
            }
        } else if (sph.getCutpriceMode() == 3 && sph.getStatus() == 1 && (sps == null || sps.size() > 0)) {
            log.info(" sph.getCutpriceMode() == 3 && sph.getStatus() == 1 && (sps == null || sps.size() > 0) 3");
            flag = 3;
        } else if (sph.getStatus() == 0) {
            log.info("3");
            flag = 3;
        } else {
            log.info("2");
            flag = 2;
        }
        return flag;
    }

    public Map<String,Integer> enterprisePaymentSpecial(LitemallRedPacaketsHistory sph, String subAppId, String userId) throws Exception {
        Map<String,Integer>  map = new HashMap<>();
        int flag = 0;
        int price = 0;
        LitemallRedPacakets platformCutprices = litemallRedPacaketsService.findById(sph.getCutpriceId());

        SysWxConfig wxConfig = systemWxConfigService.getconfigByAppletNam(subAppId);

        log.info("sph.getId():"+sph.getId()+" userId："+userId);
        ReceiveRecord receiveRecord = receiveRecordService.findReceiveRecordByBargainByBargainListIdAndOpenid(sph.getId(), userId);
        log.info("sph.getCutpriceMode():" + sph.getCutpriceMode() + " sph.getStatus():" + sph.getStatus() + " receiveRecord:" + receiveRecord);
        if ((sph.getCutpriceMode() == 4 || sph.getCutpriceMode() == 5) && (sph.getStatus() == 1 || sph.getStatus() == 0) && receiveRecord != null) {
            log.info("in=============");
            Map<String, Object> returnMap = wxGiveRedPackService.enterprisePaymentSpecial(sph, wxConfig, receiveRecord);
            flag = (int)returnMap.get("flag");
            price = (int)returnMap.get("price");
            //##领取成功后置ReceiveRecord状态
            log.info("returnMap:" + returnMap);
            if (flag == 1) {
                String tradeNo = returnMap.get("partner_trade_no").toString();
                //String tradeNo ="1234";
                receiveRecordService.updateReceiveRecordStatus(receiveRecord.getId(), 1, tradeNo);
            }
            //第一个人领取后置SysPlatformCutpriceHistory状态
            if (flag == 1 && sph.getStatus() != 0) {
                log.info("flag == 1");
                MngUserCutpricePo userCutprice = (MngUserCutpricePo) redisUntil.get(
                        "Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId());
                //log.info("userCutprice" + userCutprice);
                if (userCutprice != null) {
                    log.info("Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId() + "redis已重置");
                    userCutprice.setStatus(0);
                    redisUntil.setSeconds("Pl_set_" + sph.getCutpriceId() + "_" + platformCutprices.getAuction() + "_" + sph.getId(), userCutprice, 24 * 60 * 60);
                }
                sph.setStatus(0);
                sph.setGainTime(LocalDateTime.now());
                litemallRedPacaketsHistoryService.updateStatus(sph);
            }
        } else {
            log.info("2");
            flag = 4;
        }
        map.put("flag",flag);
        map.put("price",price);
        return map;
    }
}
