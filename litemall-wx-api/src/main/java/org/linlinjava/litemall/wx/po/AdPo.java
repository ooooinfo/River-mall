/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.wx.po;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author yica
 * @date 2019年6月15日  
 */
@Data
public class AdPo {

	private String name;

	private String link;

	private String url;




	private List<AdDanmuListPo> danmuList;



	public void setName(String name) {
		this.name = name;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setUrl(String url) {
		this.url = url;
	}




	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public String getUrl() {
		return url;
	}

	public void setDanmuList(List<AdDanmuListPo> danmuList) {
		this.danmuList = danmuList;
	}

	public List<AdDanmuListPo> getDanmuList() {
		return danmuList;
	}
}
