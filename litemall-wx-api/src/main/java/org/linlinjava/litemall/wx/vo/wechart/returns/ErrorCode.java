package org.linlinjava.litemall.wx.vo.wechart.returns;

import lombok.Data;

@Data
public class ErrorCode {
	private String errcode;
	private String errmsg;
}
