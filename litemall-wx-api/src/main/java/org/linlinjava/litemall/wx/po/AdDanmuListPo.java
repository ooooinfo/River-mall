/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.wx.po;

import lombok.Data;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author yica
 * @date 2019年6月15日  
 */
@Data
public class AdDanmuListPo {

            private String text;
            private String color;
            private Integer time;

    public void setText(String text) {
        this.text = text;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public String getColor() {
        return color;
    }

    public Integer getTime() {
        return time;
    }
}
