package org.linlinjava.litemall.wx.vo.wechart;

import lombok.Data;

@Data
public class CreateQr {
	private String path;
	private Integer width;
	private boolean auto_color;

	public void setPath(String path) {
		this.path = path;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public void setAuto_color(boolean auto_color) {
		this.auto_color = auto_color;
	}

	public String getPath() {
		return path;
	}

	public Integer getWidth() {
		return width;
	}

	public boolean isAuto_color() {
		return auto_color;
	}
}
