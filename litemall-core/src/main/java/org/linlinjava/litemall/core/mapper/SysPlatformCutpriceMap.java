/**  
 * <p>Title: SysPlatformCoupons.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年5月28日  
 */  
package org.linlinjava.litemall.core.mapper;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**  
 * <p>Title: SysPlatformCoupons</p>  
 * <p>Description: </p>  
 * @author caiy  
 * @date 2019年5月28日  
 */

@Data
public class SysPlatformCutpriceMap {
	private  Long id;
	private String batchId;//微信代金券标识
	
	private String title;//活动标题
	
	private String image;//自定义活动图片
	
	private String content;//活动详情说明
	
	private int target;//砍价目标，折扣百分比%
	
	private int status;//活动状态：1激活 2失效
	
	private int cutpriceMode;//砍价模式，1砍到目标价才能购买，2随时购买且终止砍价
	
	// private int minPerreduce;//单次最小砍价金额
	
	// private int maxPerreduce;//单次最大砍价金额
	
	private int duration;//活动持续时间，单位秒
	
	private long startTime;//

  private long endTime;

  private String subtitleSponser; //发起人看到的副标题

  private String subtitleParticipant;//参与者看到的副标题

  private int selfAvailable;//自己参与砍价：0不可以，1可以

  private int price;//原价
	
	private String userId;

	private  int auction;

  private int itemsQuota;

  // private int minTimes;
  private Integer labelUrgent;
  private int labelNew;
  private int labelEasy;
  private int top;

  private int minTimes;//最少砍价次数
  private int maxTimes;//最大砍价次数
  private int firstReduce;//第一刀占总额的百分比
  private String image2;//自定义活动图片
  private String image3;//
  private Integer deadline;//红包领取时间限制，秒数,0不限制
  private Integer selfReceivable;//0发起人不可领红包1可领
  private Integer selfPercent;//发起人固定红包百分比
  private Integer display;//1显示0不显示
  private Integer advertisingViews;//广告播放次数门限，剩余最小砍价次数小于此值时，每次砍价强制用户看广告
  private Integer receivableNum;//可领取红包的人数，0为所有参与人可领
  private String url;//自定义活动图片
  private String url2;//自定义活动图片
  private String url3;//自定义活动图片
  private int  bannerAdvertType ;//广告的类型，0图片，1视频
  private String  advertisingVideo ;
}
