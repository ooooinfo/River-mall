/**  
 * <p>Title: SysPlatformCoupons.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年5月28日  
 */  
package org.linlinjava.litemall.core.mapper;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**  
 * <p>Title: SysPlatformCoupons</p>  
 * <p>Description: </p>  
 * @author caiy  
 * @date 2019年5月28日  
 */

@Data
public class SysPlatformCutpriceHistoryMap {
     @ApiModelProperty(value = "用户openid")
	// @JsonProperty(value = "wopenId")
	private String wopenId;//用户openid
	@NotNull
    @ApiModelProperty(value = "代金券ID", required = true)
	private Long cutpriceId;//代金券ID
	
	private String appletName;
	
	private String friendId;
}
