package org.linlinjava.litemall.core.alibaba;


import com.alibaba.trade.param.AlibabaTradeFastCargo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.alibaba.po.OrderGoodsOrmPo;
import org.linlinjava.litemall.db.domain.LitemallGoodsProduct;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.service.LitemallGoodsProductService;
import org.linlinjava.litemall.db.service.LitemallOrderGoodsService;
import org.linlinjava.litemall.db.util.PurchasingUntil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


@Service
public class OrderSplitService {
    private final Log logger = LogFactory.getLog(OrderSplitService.class);
    @Autowired
    private LitemallOrderGoodsService litemallOrderGoodsService;
    @Autowired
    private LitemallGoodsProductService litemallGoodsProductService;
    @Autowired
    private AlibabaOrderService alibabaOrderService;
    /**
     *
     * 自动创建采购商订单前需要判定此订单的来源
     * 是1688 平台订单则从1688 直接下单
     * 进行订单的拆分，
     *
     * 处理逻辑
     * 根据order 获取order 详情，
     * 从order详情里面 去goods_product表获取商品，根据商品判断是否通过 1688下单
     * 存在情况 1.订单非1688货源，不操作
     *          2、订单全部是1688货源(合并下单)
     *          3.订单部分是1688货源(对1688货源进行合并下单)
     *              非1688货源订单做状态标记
     * */
   public void orderSplit(LitemallOrder order){
        //if(order==null)
        logger.info("1688发货前进行订单分拣");
        order.setPurchasingStatus(PurchasingUntil.STATUS_PURCHASING_WAIT);
       List<LitemallOrderGoods> orderGoodsList = litemallOrderGoodsService.queryByOid(order.getId());
        if(orderGoodsList!=null&& orderGoodsList.size()>0){
            logger.info("商品信息真实有效");
                List<AlibabaTradeFastCargo> cargoParamList = new ArrayList<>();
                List<LitemallOrderGoods>  newOrderGoodsList = new ArrayList<>();
            for (LitemallOrderGoods orderGoods:orderGoodsList) {


                LitemallGoodsProduct goodsProduct = litemallGoodsProductService.findById(orderGoods.getProductId());

                if (goodsProduct == null) {
                    logger.info("获取不到sku信息");
                    //return;

                } else {
                    logger.info("商品列表不为空,进行货源判断");
                    if (goodsProduct.getAlibabaProductid() != null) {
                        //是1688货源订单
                        logger.info("阿里巴巴下单入参数据拼接");
                        AlibabaTradeFastCargo cargoParam = new AlibabaTradeFastCargo();
                        cargoParam.setOfferId(goodsProduct.getAlibabaProductid());
                        //skuId和specID有可能为空
                        if (goodsProduct.getAlibabaSpecid() != null)
                            cargoParam.setSpecId(goodsProduct.getAlibabaSpecid());
                        cargoParam.setQuantity((double) orderGoods.getNumber());
                        cargoParamList.add(cargoParam);
                        newOrderGoodsList.add(orderGoods);

                    }
                }


            }
                if(cargoParamList.size()>0) {
                    logger.info("订单包含1688货源，开始下单预览");



                   // AlibabaTradeFastCargo[] data = cargoParamList.toArray(new  AlibabaTradeFastCargo[cargoParamList.size()]);

                    Map<Long,List<OrderGoodsOrmPo>> map = new HashMap<>();
                        for(int i =0 ;i<cargoParamList.size();i++){
                   // for (AlibabaTradeFastCargo str : data) {
                        OrderGoodsOrmPo orderGoodsOrmPos = new OrderGoodsOrmPo();
                        Long key = cargoParamList.get(i).getOfferId();
                            orderGoodsOrmPos.setTradeFastCargosPo(cargoParamList.get(i));
                            orderGoodsOrmPos.setLitemallOrderGoodsPo(newOrderGoodsList.get(i));
                        List<OrderGoodsOrmPo> list = map.get(key);
                        //如果map中对应key,创建数组，并加到map中
                        if(list==null||list.isEmpty()){
                            list = new ArrayList<>();
                            map.put(key,list);
                        }
                        list.add(orderGoodsOrmPos);

                    }

                    //logger.info(map);
                    //alibabaOrderService.create1688OrderAuto(list, order);
                    alibabaOrderService.createOrder4CybMedia(map,order);
                }




        }

   }




}
