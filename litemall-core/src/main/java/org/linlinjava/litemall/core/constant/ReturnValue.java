package org.linlinjava.litemall.core.constant;

import lombok.Data;

@Data
public class ReturnValue {
	public static String success = "1";
	public static String fail = "0";

	private String code;
	private String msg;
	private Object obj;

	public ReturnValue(String code, Object obj) {
		super();
		this.code = code;
		this.obj = obj;
		if (code.equals(fail)) {
			msg = obj.toString();
		}
	}
}
