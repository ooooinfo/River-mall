package org.linlinjava.litemall.core.util;
import java.sql.Timestamp;
import java.text.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.apache.commons.lang.StringUtils;


public class UncDate {
/**
 * 
 * @method getDateAfter 给指定时间加一定的小时数
 * @param date 时间
 * @param hour 小时数
 * @return Date
 * 
 */
	public static Date getDateAfter(Date date, int hour) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, hour);
		return c.getTime();
	}
	/**
	 * @method getDateCount 获取两个时间的时间差
	 * @param sDate 起始时间
	 * @param eDate 截止时间
	 * @return 时间差
	 */
	public static long getDateCount(Date sDate, Date eDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(sDate);
		long ls = c.getTimeInMillis();
		c.setTime(eDate);
		long le = c.getTimeInMillis();
		long getCnt = (le - ls) ;
		return getCnt;
	}
	/**
	 * 
	 * @param Time
	 * @param mils
	 * @return
	 * @author caiy
	 * 2019/4/4
	 */
	public static String getDate(long Time,long mils) {
		long currentTime =  Time + mils;

		Date date = new Date(currentTime);

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String nowTime="";
		nowTime= df.format(date);
		return nowTime;
				
	}
	/**
	 * 算出现在至明日0点的分钟差
	 * @return
	 * @author caiy
	 * 2019/4/16
	 */
	public static Long getTimeout() {
		Calendar calendar  = Calendar.getInstance();
		calendar.add(calendar.DAY_OF_YEAR, 1);
		calendar.set(calendar.HOUR_OF_DAY, 0);
		calendar.set(calendar.SECOND, 0);
		calendar.set(calendar.MINUTE,0);
		calendar.set(calendar.MILLISECOND,0);
		long a = calendar.getTimeInMillis();
		long b = System.currentTimeMillis();
		long minute = (a-b)/(1000*60);
		//long minute = (calendar.getTimeInMillis()-System.currentTimeMillis())/(1000*60);
		return minute;
	}
	/**
	 * 时间戳转日期 2019年 04月17日  15:17:55
	 * @param timeStamp
	 * @return
	 * @author caiy
	 * 2019/4/17
	 */
	public static String timeStampToDate(long timeStamp) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年 MM月dd日  HH:mm:ss");//这个是你要转成后的时间的格式
		String date = sdf.format(new Date(timeStamp));
		// System.out.println(date);
    //log.info(date);
		return date;
		
	}
	
	public static Date timeToDate(long timeStamp) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年 MM月dd日  HH:mm:ss");//这个是你要转成后的时间的格式
		Date date = new Date(timeStamp);
		// System.out.println(date);
    //log.info(date);
		return date;
		
	}

	/**
	 * 计算两个日志之间相隔天数
	 * 
	 * @param sDate
	 *            开始日期
	 * @param eDate
	 *            结束日期
	 * @return
	 */
	public static int getDayCount(Date sDate, Date eDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(sDate);
		long ls = c.getTimeInMillis();
		c.setTime(eDate);
		long le = c.getTimeInMillis();
		int getCnt = (int) ((le - ls) / (24 * 3600 * 1000));
		return getCnt;
	}
  /**
   * 获取当天零点时间
   *
   * @return
   */
  public static long getTimeToDay() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    Date zero = calendar.getTime();
    return zero.getTime();
  }
  public static String dateToString(Date time) {
	  String s = "";
	  DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	  s = format1.format(time);
	  return s;
  }
  /**
   * 计算两个时间戳之差
   * <p>Description: </p>
   * @param sDate
   * @param eDate
   * @return
   * @author caiy  
   * @date 2019年5月6日
   */
  public static long getDateCount(long sDate, long eDate) {
		long getCnt = (eDate - sDate) ;
		return getCnt;
	}
  /**
   * 计算今天0点
   * <p>Description: </p>
   * @param
   * @param
   * @return
   * @author caiy  
   * @date 2019年5月6日
   */
  public static long getTodayStart() {
	  Long time = System.currentTimeMillis(); 
	  //当前时间的时间戳 
	  long zero = time/(1000*3600*24)*(1000*3600*24) - TimeZone.getDefault().getRawOffset();

	  return zero;
	}
  /**
   * 计算今天23点59分59秒的时间戳
   * <p>Description: </p>
   * @param
   * @param
   * @return
   * @author caiy  
   * @date 2019年5月6日
   */
  public static long getTodayEnd() {
	  Calendar calendar = Calendar.getInstance(); 
	  calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),23,59,59); 
	  long tt = calendar.getTime().getTime()/1000;
	  return tt/1000;
	}
  public static String getDate() {
	  Calendar calendar = Calendar.getInstance();
	  calendar.add(Calendar.DATE, -1); //得到前一天
	  Date date = calendar.getTime();
	  DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	  String datepre = df.format(date);
	  datepre+= " 00:00:00";
	  return df.format(date);
	}
  /**
   * 	计算昨天23:59:59 秒的时间戳
   * <p>Description: </p>
   * @param
   * @param
   * @return
   * @author caiy  
   * @date 2019年5月6日
   */
  public static long getYesterdayEnd() {
	  Calendar calendar = Calendar.getInstance(); 
	  calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)-1,23,59,59);
	  long tt = calendar.getTime().getTime(); 
	  return tt;
	}


	public static Date getTime(int hour){
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date=new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.HOUR, hour);
    date = calendar.getTime();
    //log.info(sdf.format(date));
    return date;
  }

  /**
   * @Description: getPreMonthTime 获取一个月前的当前时间
   * @param: []
   * @return: java.lang.String
   * @auther: caiy
   * @date: 2019/7/31 0031 15:23
   */
  public static String getPreMonthTime(){
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.MONTH, -1);
    Date m = c.getTime();
    String mon = format.format(m);
    return  mon;
  }
  public static Date transferLongToDate(Long millSec){

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    Date date= new Date(millSec);
    return date;

  }

  /**
   * 获取当月第一天的零点
   * @return
   * @Auther: wangly
   * @Date: 2019/7/18 0018 上午 10:11
   */
  public static long getMonthZeroTime(){
    // 获取当月第一天
    Calendar  cale = Calendar.getInstance();
    cale.add(Calendar.MONTH, 0);
    cale.set(Calendar.DAY_OF_MONTH, 1);
    cale.set(Calendar.HOUR_OF_DAY, 0);
    cale.set(Calendar.MINUTE, 0);
    cale.set(Calendar.SECOND, 0);
    long time = cale.getTime().getTime();
    return time;
  }
  public static Date getNowDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        ParsePosition pos = new ParsePosition(8);
       Date currentTime_2 = formatter.parse(dateString, pos);
       return currentTime_2;
     }
  public static Date strToDateLong(String strDate) {
     SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
     }
	public static void main(String[] args) throws ParseException {

    Date time = UncDate.strToDateLong("2019-08-23 17:06:23");
		System.out.println(time);
	}


	public static long getDateCount(LocalDateTime startTime, LocalDate endTime) {
  		Long endTimes  = endTime.atStartOfDay(ZoneOffset.ofHours(8)).toInstant().toEpochMilli();
  		Long startTimes = startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
		long getCnt = (endTimes - startTimes) ;
		return getCnt;
	}
	public static long getDateCountNew(LocalDateTime startTime, LocalDateTime endTime) {
		//Long endTimes  = endTime.atStartOfDay(ZoneOffset.ofHours(8)).toInstant().toEpochMilli();
		Long endTimes = endTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
		Long startTimes = startTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
		long getCnt = (endTimes - startTimes) ;
		return getCnt;
	}
}
