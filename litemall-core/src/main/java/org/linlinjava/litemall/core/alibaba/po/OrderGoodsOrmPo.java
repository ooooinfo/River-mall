/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.core.alibaba.po;

import com.alibaba.trade.param.AlibabaTradeFastCargo;
import lombok.Data;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author yica
 * @date 2019年6月15日  
 */
@Data
public class OrderGoodsOrmPo {
	private AlibabaTradeFastCargo  tradeFastCargosPo;
	private LitemallOrderGoods   litemallOrderGoodsPo;

	public void setTradeFastCargosPo(AlibabaTradeFastCargo tradeFastCargosPo) {
		this.tradeFastCargosPo = tradeFastCargosPo;
	}

	public AlibabaTradeFastCargo getTradeFastCargosPo() {
		return tradeFastCargosPo;
	}

	public void setLitemallOrderGoodsPo(LitemallOrderGoods litemallOrderGoodsPo) {
		this.litemallOrderGoodsPo = litemallOrderGoodsPo;
	}


	public LitemallOrderGoods getLitemallOrderGoodsPo() {
		return litemallOrderGoodsPo;
	}

	public OrderGoodsOrmPo(){

	}

}
