package org.linlinjava.litemall.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "litemall.wx")
public class WxProperties {

    private String appId; //服务商的服务号appid
    private String subAppId; //子商户小程序appid
    private String subMchId;  //子商户的商户号

    private String subAppSecret; //子商户小程序的appsecret

    private String mchId;  //  服务商商户号

    private String mchKey;  // 服务商商户对应的key

    private String notifyUrl;  // 回调地址

    private String keyPath;  // 证书文件存放目录



    public String getSubAppId(){
        return subAppId;
    }
    public  void  setSubAppId(String subAppId){
        this.subAppId = subAppId;
    }
    public String getSubMchId(){
        return  subMchId;
    }
    public void  setSubMchId(String subMchId){
        this.subMchId = subMchId;
    }
    public String getSubAppSecret(){
        return subAppSecret;
    }
    public void  setSubAppSecret(String subAppSecret){
        this.subAppSecret = subAppSecret;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getMchKey() {
        return mchKey;
    }

    public void setMchKey(String mchKey) {
        this.mchKey = mchKey;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }



    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getKeyPath() {
        return keyPath;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }
}
