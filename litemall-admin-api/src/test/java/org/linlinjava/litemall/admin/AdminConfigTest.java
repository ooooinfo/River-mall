package org.linlinjava.litemall.admin;

import com.alibaba.ocean.rawsdk.ApiExecutor;
import com.alibaba.product.param.AlibabaProductFollowParam;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.linlinjava.litemall.core.validator.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.bind.annotation.RequestParam;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AdminConfigTest {
    @Autowired
    private Environment environment;

    @Test
    public void test() {
        // 测试获取application-core.yml配置信息
        //System.out.println(environment.getProperty("litemall.express.appId"));
        // 测试获取application-db.yml配置信息
       // System.out.println(environment.getProperty("spring.datasource.druid.url"));
        // 测试获取application-admin.yml配置信息
        // System.out.println(environment.getProperty(""));
        // 测试获取application.yml配置信息
       // System.out.println(environment.getProperty("logging.level.org.linlinjava.litemall.admin"));
        //@Order @RequestParam(defaultValue = "desc") String order) {
            ApiExecutor apiExecutor = new ApiExecutor("2794560", "aYyuPwfCbi3");
            AlibabaProductFollowParam param = new AlibabaProductFollowParam();
            Long productId = Long.parseLong("600559711625");
            param.setProductId(productId);
            System.out.println(apiExecutor.execute(param));
    }

}
