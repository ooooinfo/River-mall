package org.linlinjava.litemall.admin.alibaba;

import org.linlinjava.litemall.admin.alibaba.po.AlibabaCpsOpListCybUserGroupFeedListResult;

public class AlibabaCpsOpListCybUserGroupFeedResult {


    private Integer totalRow;
    //private Boolean success;
    private AlibabaCpsOpListCybUserGroupFeedListResult[] resultList;


    public Integer getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Integer totalRow) {
        this.totalRow = totalRow;
    }

    public void setResultList(AlibabaCpsOpListCybUserGroupFeedListResult[] resultList) {
        this.resultList = resultList;
    }

    public AlibabaCpsOpListCybUserGroupFeedListResult[] getResultList() {
        return resultList;
    }
}
