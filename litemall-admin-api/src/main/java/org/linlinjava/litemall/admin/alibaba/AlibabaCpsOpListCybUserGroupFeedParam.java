package org.linlinjava.litemall.admin.alibaba;

import com.alibaba.ocean.rawsdk.client.APIId;
import com.alibaba.ocean.rawsdk.common.AbstractAPIRequest;


public class AlibabaCpsOpListCybUserGroupFeedParam extends AbstractAPIRequest<Object> {



    private Long groupId;

    private Integer pageNo;

    private Integer pageSize;
    public AlibabaCpsOpListCybUserGroupFeedParam() {
        super();
        oceanApiId = new APIId("com.alibaba.p4p", "alibaba.cps.op.listCybUserGroupFeed", 1);
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getGroupId() {
        return groupId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }
}
