package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.admin.service.AdminOrderService;
import org.linlinjava.litemall.admin.vo.OrdersVo;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/admin/order")
@Validated
public class AdminOrderController {
    private final Log logger = LogFactory.getLog(AdminOrderController.class);

    @Autowired
    private AdminOrderService adminOrderService;


    /**
     * 查询订单
     *
     * @param userId
     * @param orderSn
     * @param orderStatusArray
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:order:list")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(Integer userId, String orderSn,
                       @RequestParam(required = false) List<Short> orderStatusArray,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
         Subject currentUser = SecurityUtils.getSubject();
         LitemallAdmin admin = (LitemallAdmin) currentUser.getPrincipal();

         List<LitemallOrder> list =adminOrderService.list(userId, orderSn, orderStatusArray, page, limit, sort, order,admin.getId());
         List<OrdersVo> ordersVoList= new ArrayList<>();
         for(LitemallOrder gordes:list){
                OrdersVo ordersVo = new OrdersVo();
                ordersVo.setId(gordes.getId());
                ordersVo.setUserId(gordes.getUserId());
                ordersVo.setOrderSn(gordes.getOrderSn());
                ordersVo.setOrderStatus(gordes.getOrderStatus());
                ordersVo.setConsignee(gordes.getConsignee());
                ordersVo.setMobile(gordes.getMobile());
                ordersVo.setAddress(gordes.getAddress());
                ordersVo.setMessage(gordes.getMessage());
                ordersVo.setGoodsPrice(gordes.getGoodsPrice());
                ordersVo.setFreightPrice(gordes.getFreightPrice());
                ordersVo.setCouponPrice(gordes.getCouponPrice());
                ordersVo.setIntegralPrice(gordes.getIntegralPrice());
                ordersVo.setGrouponPrice(gordes.getGrouponPrice());
                ordersVo.setOrderPrice(gordes.getOrderPrice());
                ordersVo.setActualPrice(gordes.getActualPrice());
                ordersVo.setPayId(gordes.getPayId());
                ordersVo.setPayTime(gordes.getPayTime());
                ordersVo.setShipSn(gordes.getShipSn());
                ordersVo.setShipChannel(gordes.getShipChannel());
                ordersVo.setShipTime(gordes.getShipTime());
                ordersVo.setConfirmTime(gordes.getConfirmTime());
                ordersVo.setComments(gordes.getComments());
                ordersVo.setEndTime(gordes.getEndTime());
                ordersVo.setAddTime(gordes.getAddTime());
                ordersVo.setUpdateTime(gordes.getUpdateTime());
                ordersVo.setAlibabaOrderid(gordes.getAlibabaOrderid());
                ordersVo.setPurchasingStatus(gordes.getPurchasingStatus());
                ordersVo.setPurchasingFailReason(gordes.getPurchasingFailReason());
                ordersVo.setPurchasingPrice(gordes.getPurchasingPrice());
                ordersVo.setLevel("LV1");

                List<LitemallOrderGoods> orderGoodsList = adminOrderService.orderGoodsList(gordes.getId());
                List<OrdersVo> children = new ArrayList<>();
                for(LitemallOrderGoods lgoods:orderGoodsList){

                    OrdersVo subordersVo = new OrdersVo();
                    subordersVo.setId(lgoods.getId());
                    subordersVo.setUserId(gordes.getUserId());
                    subordersVo.setOrderSn(lgoods.getAlibabaOrderid());
                    subordersVo.setOrderStatus(gordes.getOrderStatus());
                    subordersVo.setConsignee(gordes.getConsignee());
                    subordersVo.setMobile(gordes.getMobile());
                    subordersVo.setAddress(gordes.getAddress());
                    subordersVo.setMessage(gordes.getMessage());
                    subordersVo.setGoodsPrice(lgoods.getPrice());
                   // subordersVo.setFreightPrice(lgoods.getFreightPrice());
                   // subordersVo.setCouponPrice(lgoods.getCouponPrice());
                   // subordersVo.setIntegralPrice(lgoods.getIntegralPrice());
                   // subordersVo.setGrouponPrice(lgoods.getGrouponPrice());
                    subordersVo.setOrderPrice(lgoods.getPrice());
                    subordersVo.setActualPrice(lgoods.getPrice());
                    subordersVo.setPayId(gordes.getPayId());
                    subordersVo.setPayTime(gordes.getPayTime());
                    subordersVo.setShipSn(lgoods.getShipSn());
                    subordersVo.setShipChannel(lgoods.getShipChannel());
                    subordersVo.setShipTime(lgoods.getShipTime());
                    subordersVo.setConfirmTime(gordes.getConfirmTime());
                    subordersVo.setComments(gordes.getComments());
                    subordersVo.setEndTime(gordes.getEndTime());
                    subordersVo.setAddTime(lgoods.getAddTime());
                    subordersVo.setUpdateTime(lgoods.getUpdateTime());
                    subordersVo.setAlibabaOrderid(lgoods.getAlibabaOrderid());
                    subordersVo.setPurchasingStatus(lgoods.getPurchasingStatus());
                    subordersVo.setPurchasingFailReason(lgoods.getPurchasingFailReason());
                    subordersVo.setPurchasingPrice(lgoods.getPruchasingPrice());
                    subordersVo.setLevel("LV2");

                    children.add(subordersVo);

                }
                ordersVo.setChildren(children);
                ordersVoList.add(ordersVo);
            }
        return ResponseUtil.okList(ordersVoList);

    }

    /**
     * 订单详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:order:read")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull Integer id) {
        return adminOrderService.detail(id);
    }
    /**
     * 创建1688订单
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:order:create1688")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "1688下单")
    @GetMapping("/to1688Order")
    public Object to1688Order(@NotNull Integer id) {
        return adminOrderService.to1688Order(id);
    }
    /**
     * 订单退款
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @RequiresPermissions("admin:order:refund")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "订单退款")
    @PostMapping("/refund")
    public Object refund(@RequestBody String body) {
        return adminOrderService.refund(body);
    }

    /**
     * 发货
     *
     * @param body 订单信息，{ orderId：xxx, shipSn: xxx, shipChannel: xxx }
     * @return 订单操作结果
     */
    @RequiresPermissions("admin:order:ship")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "订单发货")
    @PostMapping("/ship")
    public Object ship(@RequestBody String body) {
        return adminOrderService.ship(body);
    }


    /**
     * 回复订单商品
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单操作结果
     */
    @RequiresPermissions("admin:order:reply")
    @RequiresPermissionsDesc(menu = {"商场管理", "订单管理"}, button = "订单商品回复")
    @PostMapping("/reply")
    public Object reply(@RequestBody String body) {
        return adminOrderService.reply(body);
    }

}
