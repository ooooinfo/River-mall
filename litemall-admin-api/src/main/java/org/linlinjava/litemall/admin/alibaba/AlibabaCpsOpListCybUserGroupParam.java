package org.linlinjava.litemall.admin.alibaba;

import com.alibaba.ocean.rawsdk.client.APIId;
import com.alibaba.ocean.rawsdk.common.AbstractAPIRequest;
import com.alibaba.product.param.AlibabaCategoryGetResult;

public class AlibabaCpsOpListCybUserGroupParam extends AbstractAPIRequest<AlibabaCpsOpListCybUserGroupResult> {

    public AlibabaCpsOpListCybUserGroupParam() {
        super();
        oceanApiId = new APIId("com.alibaba.p4p", "alibaba.cps.op.listCybUserGroup", 1);
    }



    private String pageNo;

    private String pageSize;

    //private String access_token;



    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }



    public String getPageNo() {
        return pageNo;
    }

    public String getPageSize() {
        return pageSize;
    }
}
