package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.admin.service.AdminPddService;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/admin/pdd")
@Validated
public class AdminPddController {
    private final Log logger = LogFactory.getLog(AdminPddController.class);

    @Autowired
    private AdminPddService adminPddService;

    @RequiresPermissions("admin:pdd:scnypdd")
    @RequiresPermissionsDesc(menu = {"拼多多同步", "单商品同步"}, button = "同步")
    @GetMapping("/scnypdd")
    public Object scnypdd(Integer id) throws Exception {

           return adminPddService.goodsAdd(id);
       // adminPddService.updateCateList();
       // return ResponseUtil.ok();
    }


}
