package org.linlinjava.litemall.admin.alibaba.vo;

import java.util.Date;

public class AlibabaMessageGoodsSkuVo {
        private String offerId;
        private String offerOnSale;//在线可售offer数量
        private String skuId;
        private String skuOnSale;//在线可售sku数量
        private String quantity;///整体库存变化
        private String bizTime;//变更时间 示例：1564984329147

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public void setOfferOnSale(String offerOnSale) {
        this.offerOnSale = offerOnSale;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public void setSkuOnSale(String skuOnSale) {
        this.skuOnSale = skuOnSale;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setBizTime(String bizTime) {
        this.bizTime = bizTime;
    }

    public String getOfferId() {
        return offerId;
    }

    public String getOfferOnSale() {
        return offerOnSale;
    }

    public String getSkuId() {
        return skuId;
    }

    public String getSkuOnSale() {
        return skuOnSale;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getBizTime() {
        return bizTime;
    }
}

