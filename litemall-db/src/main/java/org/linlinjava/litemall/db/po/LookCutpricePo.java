package org.linlinjava.litemall.db.po;

import org.linlinjava.litemall.db.domain.Articles;
import org.linlinjava.litemall.db.po.Image;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Auther: wangly
 * @Date: 2019/5/29 0029 09:02
 */
@Data
@NoArgsConstructor
public class LookCutpricePo implements Serializable {
  private Long userCutpriceId;//用户发起砍价活动id
  private int owner;//1平台砍价活动，2店铺砍价活动
  private Long storeId;//店铺id
  private Long orderId;
  private Integer totalReducation;//目前总减价额
  private Integer needCutvalue;//需要砍减的目标金额
  private String subtitleSponser;//发起人看到的副标题  邀请好友，分享助力，赢取商家赠送菜品或礼物！
  private String subtitleParticipant;//参与者看到的副标题
  private String userId;//参与者看到的副标题
  private long time;//剩余时间
  private Integer status;//1.完成  2.进行中 3.失效 0是已领取
  private Long platformCutpriceId;//砍价活动id
  private Long platformCutpriceHistoryId;//砍价活动id
  private Integer price;
  private Integer duration;//活动持续时间，单位秒
  private Integer actualprice;
  private Integer paystatus;
  private Integer selfAvailable;
  private LocalDateTime createTime;//发起砍价活动时间
  private String title;
  private String image;
  private String image2;
  private String image3;
  private String url;
  private String url2;
  private String url3;
  private Integer cutpriceMode;//砍价模式，1砍到目标价才能购买，2随时购买且终止砍价
  private Integer isSubscribe;
  private Integer advertisingViews;
  private Integer minTimes;

  private int isPlay;//0是未达到门限，1是达到门限
  private Integer auction;//
  private Integer selfRceivable;
  private Integer selfStatus;//是否已领取
  private Articles articles;
  private List<UserListPo> userList;//领取列表
  private List<ActivityImage> activityImages;//
  private List<String> receivableUserIds;
  private List<ConsumerPo> consumerList = new ArrayList<>();
  private MngCoupon mngCoupon;//参与砍价者获赠的优惠券
  private int isSelf;//用户查看时判断是否是自己发出的砍价活动,0不是，1是
  private Integer isCutprice;

  private int  bannerAdvertType ;//广告的类型，0图片，1视频
  private String  advertisingVideo ;


  public void setUserCutpriceId(Long userCutpriceId) {
    this.userCutpriceId = userCutpriceId;
  }

  public void setOwner(int owner) {
    this.owner = owner;
  }

  public void setStoreId(Long storeId) {
    this.storeId = storeId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public void setTotalReducation(Integer totalReducation) {
    this.totalReducation = totalReducation;
  }

  public void setNeedCutvalue(Integer needCutvalue) {
    this.needCutvalue = needCutvalue;
  }

  public void setSubtitleSponser(String subtitleSponser) {
    this.subtitleSponser = subtitleSponser;
  }

  public void setSubtitleParticipant(String subtitleParticipant) {
    this.subtitleParticipant = subtitleParticipant;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public void setPlatformCutpriceId(Long platformCutpriceId) {
    this.platformCutpriceId = platformCutpriceId;
  }

  public void setPlatformCutpriceHistoryId(Long platformCutpriceHistoryId) {
    this.platformCutpriceHistoryId = platformCutpriceHistoryId;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public void setActualprice(Integer actualprice) {
    this.actualprice = actualprice;
  }

  public void setPaystatus(Integer paystatus) {
    this.paystatus = paystatus;
  }

  public void setSelfAvailable(Integer selfAvailable) {
    this.selfAvailable = selfAvailable;
  }

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public void setImage2(String image2) {
    this.image2 = image2;
  }

  public void setImage3(String image3) {
    this.image3 = image3;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public void setUrl2(String url2) {
    this.url2 = url2;
  }

  public void setUrl3(String url3) {
    this.url3 = url3;
  }

  public void setCutpriceMode(Integer cutpriceMode) {
    this.cutpriceMode = cutpriceMode;
  }

  public void setIsSubscribe(Integer isSubscribe) {
    this.isSubscribe = isSubscribe;
  }

  public void setAdvertisingViews(Integer advertisingViews) {
    this.advertisingViews = advertisingViews;
  }

  public void setMinTimes(Integer minTimes) {
    this.minTimes = minTimes;
  }

  public void setIsPlay(int isPlay) {
    this.isPlay = isPlay;
  }

  public void setAuction(Integer auction) {
    this.auction = auction;
  }

  public void setSelfRceivable(Integer selfRceivable) {
    this.selfRceivable = selfRceivable;
  }

  public void setSelfStatus(Integer selfStatus) {
    this.selfStatus = selfStatus;
  }

  public void setArticles(Articles articles) {
    this.articles = articles;
  }

  public void setUserList(List<UserListPo> userList) {
    this.userList = userList;
  }

  public void setActivityImages(List<ActivityImage> activityImages) {
    this.activityImages = activityImages;
  }

  public void setReceivableUserIds(List<String> receivableUserIds) {
    this.receivableUserIds = receivableUserIds;
  }

  public void setConsumerList(List<ConsumerPo> consumerList) {
    this.consumerList = consumerList;
  }

  public void setMngCoupon(MngCoupon mngCoupon) {
    this.mngCoupon = mngCoupon;
  }

  public void setIsSelf(int isSelf) {
    this.isSelf = isSelf;
  }

  public void setIsCutprice(Integer isCutprice) {
    this.isCutprice = isCutprice;
  }

  public void setBannerAdvertType(int bannerAdvertType) {
    this.bannerAdvertType = bannerAdvertType;
  }

  public void setAdvertisingVideo(String advertisingVideo) {
    this.advertisingVideo = advertisingVideo;
  }

  public Long getUserCutpriceId() {
    return userCutpriceId;
  }

  public int getOwner() {
    return owner;
  }

  public Long getStoreId() {
    return storeId;
  }

  public Long getOrderId() {
    return orderId;
  }

  public Integer getTotalReducation() {
    return totalReducation;
  }

  public Integer getNeedCutvalue() {
    return needCutvalue;
  }

  public String getSubtitleSponser() {
    return subtitleSponser;
  }

  public String getSubtitleParticipant() {
    return subtitleParticipant;
  }

  public String getUserId() {
    return userId;
  }

  public long getTime() {
    return time;
  }

  public Integer getStatus() {
    return status;
  }

  public Long getPlatformCutpriceId() {
    return platformCutpriceId;
  }

  public Long getPlatformCutpriceHistoryId() {
    return platformCutpriceHistoryId;
  }

  public Integer getPrice() {
    return price;
  }

  public Integer getDuration() {
    return duration;
  }

  public Integer getActualprice() {
    return actualprice;
  }

  public Integer getPaystatus() {
    return paystatus;
  }

  public Integer getSelfAvailable() {
    return selfAvailable;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }

  public String getTitle() {
    return title;
  }

  public String getImage() {
    return image;
  }

  public String getImage2() {
    return image2;
  }

  public String getImage3() {
    return image3;
  }

  public String getUrl() {
    return url;
  }

  public String getUrl2() {
    return url2;
  }

  public String getUrl3() {
    return url3;
  }

  public Integer getCutpriceMode() {
    return cutpriceMode;
  }

  public Integer getIsSubscribe() {
    return isSubscribe;
  }

  public Integer getAdvertisingViews() {
    return advertisingViews;
  }

  public Integer getMinTimes() {
    return minTimes;
  }

  public int getIsPlay() {
    return isPlay;
  }

  public Integer getAuction() {
    return auction;
  }

  public Integer getSelfRceivable() {
    return selfRceivable;
  }

  public Integer getSelfStatus() {
    return selfStatus;
  }

  public Articles getArticles() {
    return articles;
  }

  public List<UserListPo> getUserList() {
    return userList;
  }

  public List<ActivityImage> getActivityImages() {
    return activityImages;
  }

  public List<String> getReceivableUserIds() {
    return receivableUserIds;
  }

  public List<ConsumerPo> getConsumerList() {
    return consumerList;
  }

  public MngCoupon getMngCoupon() {
    return mngCoupon;
  }

  public int getIsSelf() {
    return isSelf;
  }

  public Integer getIsCutprice() {
    return isCutprice;
  }

  public int getBannerAdvertType() {
    return bannerAdvertType;
  }

  public String getAdvertisingVideo() {
    return advertisingVideo;
  }
}
