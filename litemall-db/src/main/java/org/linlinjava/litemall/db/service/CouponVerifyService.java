package org.linlinjava.litemall.db.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallCart;
import org.linlinjava.litemall.db.domain.LitemallCoupon;
import org.linlinjava.litemall.db.domain.LitemallCouponUser;
import org.linlinjava.litemall.db.util.CouponConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

@Service
public class CouponVerifyService {

    @Autowired
    private LitemallCouponUserService couponUserService;
    @Autowired
    private LitemallCouponService couponService;

    private final Log logger = LogFactory.getLog(CouponVerifyService.class);
    /**
     * 检测购物车里面商品是否包含此单品券适用商品
     * */
    public Boolean checkSigleCoupon(List<LitemallCart> checkedGoodsList,LitemallCoupon coupon){
        Boolean flag = false;
        if(coupon!=null&&coupon.getGoodsType()==CouponConstant.GOODS_TYPE_ARRAY){
            //单品类优惠券
            for(LitemallCart cart : checkedGoodsList){
                for(int i=0;i<coupon.getGoodsValue().length;i++){
                    logger.info(coupon.getGoodsValue()[i]);
                    logger.info("goodsId:"+cart.getGoodsId());
                    if(cart.getGoodsId().intValue()==coupon.getGoodsValue()[i].intValue()){
                        flag= true;
                      //  break;
                    }
                }

            }
        }else if(coupon!=null&&coupon.getGoodsType()==CouponConstant.GOODS_TYPE_ALL){
            //全场代金券且不为空
            flag=true;
        }
        return flag;
    }
    /**
     * 检测优惠券是否适合
     *
     * @param userId
     * @param couponId
     * @param checkedGoodsPrice
     * @return
     */
    public LitemallCoupon checkCoupon(Integer userId, Integer couponId, Integer userCouponId, BigDecimal checkedGoodsPrice) {
        LitemallCoupon coupon = couponService.findById(couponId);
        if (coupon == null) {
            logger.info("优惠券不存在");
            return null;
        }

        LitemallCouponUser couponUser = couponUserService.findById(userCouponId);
        if (couponUser == null) {

            couponUser = couponUserService.queryOne(userId, couponId);
        } else if (!couponId.equals(couponUser.getCouponId())) {
            logger.info("用户持有的优惠券和核销优惠券不对应");
            return null;
        }

        if (couponUser == null) {
            logger.info("用户未持有此优惠券");
            return null;
        }

        // 检查是否超期
        Short timeType = coupon.getTimeType();
        Short days = coupon.getDays();
        LocalDateTime now = LocalDateTime.now();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            if (now.isBefore(coupon.getStartTime()) || now.isAfter(coupon.getEndTime())) {
                logger.info("优惠券已经失效");
                return null;
            }
        }
        else if(timeType.equals(CouponConstant.TIME_TYPE_DAYS)) {
            LocalDateTime expired = couponUser.getAddTime().plusDays(days);
            if (now.isAfter(expired)) {
                logger.info("超出核销期限");
                return null;
            }
        }
        else {
            logger.info("时间格式不正确");
            return null;
        }

        // 检测商品是否符合
        // TODO 目前仅支持全平台商品，所以不需要检测
        Short goodType = coupon.getGoodsType();
        logger.info("检测此订单是否可用此优惠券开始");
        if (!goodType.equals(CouponConstant.GOODS_TYPE_ALL)) {
            logger.info("优惠券非全平台券");
            if(goodType.equals(CouponConstant.GOODS_TYPE_CATEGORY)){
                logger.info("此优惠券是品类券");
                return null;
            }else if(goodType.equals(CouponConstant.GOODS_TYPE_ARRAY)){
                logger.info("此优惠券是单品券");
                /**
                 * 此处只判断此用户是否持有该券且券存在
                 * */
                //return coupon;
            }

        }

        // 检测订单状态
        Short status = coupon.getStatus();
        if (!status.equals(CouponConstant.STATUS_NORMAL)) {
            logger.info("订单状态异常");
            return null;
        }
        // 检测是否满足最低消费
        if (checkedGoodsPrice.compareTo(coupon.getMin()) == -1) {
            logger.info("不满足最低消费");
            return null;
        }

        return coupon;
    }

}