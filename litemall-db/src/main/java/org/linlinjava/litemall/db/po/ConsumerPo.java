package org.linlinjava.litemall.db.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 食客
 *
 * @Auther: wangly
 * @Date: 2019/5/29 0029 上午 10:01
 */
@Data
public class ConsumerPo implements Serializable {

  private String userId;//微信openid

  private String nickName;//微信昵称

  private Byte gender;//性别

  private String avatarUrl;

  private int cutprice;

}
