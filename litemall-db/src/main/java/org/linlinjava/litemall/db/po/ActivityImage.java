package org.linlinjava.litemall.db.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @ProjectName: diesa_server
 * @Package: com.tmxk.diesa.po
 * @ClassName: ActivityImage
 * @Author: Administrator
 * @Description: ${description}
 * @Date: 2019/8/6 0006 11:28
 * @Version:
 */
@Data
public class ActivityImage  implements Serializable {
  private String image;//图片
  private String url;//链接
  private int defaut = 0;//是否默认显示，0否；1是
}
