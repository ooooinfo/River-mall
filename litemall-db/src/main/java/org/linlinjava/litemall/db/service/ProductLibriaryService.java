package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.ArticlesMapper;
import org.linlinjava.litemall.db.dao.ProductLibrraryMapper;
import org.linlinjava.litemall.db.domain.Articles;
import org.linlinjava.litemall.db.domain.ProductLibrrary;
import org.linlinjava.litemall.db.domain.ProductLibrraryExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductLibriaryService {
    @Resource
    private ProductLibrraryMapper productLibrraryMapper;

    public void add(ProductLibrrary productLibrrary){

        productLibrraryMapper.insert(productLibrrary);
    }
    public void validAndAdd(ProductLibrrary productLibrrary){
        ProductLibrraryExample example = new ProductLibrraryExample();
        example.or().andProductLibraryIdEqualTo(productLibrrary.getProductLibraryId());
        List<ProductLibrrary> list = productLibrraryMapper.selectByExample(example);
        if(list!=null&&list.size()>0)
            return;
        productLibrraryMapper.insert(productLibrrary);
    }
    public List<ProductLibrrary> getAll(){
        ProductLibrraryExample example = new ProductLibrraryExample();
        return productLibrraryMapper.selectByExample(example);
    }

}
