package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.ReceiveRecordMapper;
import org.linlinjava.litemall.db.dao.SysConfigMapper;
import org.linlinjava.litemall.db.domain.ReceiveRecord;
import org.linlinjava.litemall.db.domain.ReceiveRecordExample;
import org.linlinjava.litemall.db.domain.SysConfig;
import org.linlinjava.litemall.db.domain.SysConfigExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SystemConfigService {
    @Resource
    private SysConfigMapper sysConfigMapper;

    public SysConfig findByKey(String key){
        SysConfigExample example = new SysConfigExample();
        example.or().andConfKeyEqualTo(key);
        return sysConfigMapper.selectOneByExample(example);
    }


}
