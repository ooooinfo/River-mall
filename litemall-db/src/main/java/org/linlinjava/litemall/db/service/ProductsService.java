package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.ProductLibrraryMapper;
import org.linlinjava.litemall.db.dao.ProductsMapper;
import org.linlinjava.litemall.db.domain.ProductLibrrary;
import org.linlinjava.litemall.db.domain.ProductLibrraryExample;
import org.linlinjava.litemall.db.domain.Products;
import org.linlinjava.litemall.db.domain.ProductsExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductsService {
    @Resource
    private ProductsMapper productsMapper;

    public void add(Products products){

        productsMapper.insert(products);
    }
    public void delete(Integer id){
        ProductsExample example = new ProductsExample();
        example.or().andIdEqualTo(id);
        productsMapper.deleteByExample(example);
    }
    public void update(Products products,String feedId){
        ProductsExample example = new ProductsExample();
        example.or().andFeedIdEqualTo(feedId);
       // Products products = productsMapper.selectOneByExample(example);
        productsMapper.updateByExampleSelective(products,example);
    }
    public void validAndAdd(Products products){
        ProductsExample example = new ProductsExample();
        example.or().andFeedIdEqualTo(products.getFeedId());
        List<Products> list = productsMapper.selectByExample(example);
        if(list!=null&&list.size()>0){
            return;
        }
        productsMapper.insert(products);
    }
    /**
     * 查询类库下的商品
     * */
    public List<Products> getProductByProductId(Long libraryid,Integer status,Integer page,Integer size){
        ProductsExample example = new ProductsExample();
       /// example.or().andLibraryIdEqualTo(libraryid.intValue());
       // ProductsExample.Criteria criteria= example.or();
        if(status!=null){
            example.or().andLibraryIdEqualTo(libraryid.intValue()).andStatusEqualTo(status);
        }else {
            example.or().andLibraryIdEqualTo(libraryid.intValue());
        }
        PageHelper.startPage(page, size);
        return productsMapper.selectByExample(example);
    }

    public Products getProductOneByOfferId(String feedId){
        ProductsExample example = new ProductsExample();
        example.or().andFeedIdEqualTo(feedId);
        return productsMapper.selectOneByExample(example);
    }

    /**获取总数*/
    public Integer getProductCount(Long libraryid,Integer status){
        ProductsExample example = new ProductsExample();
        if(status!=null){
            example.or().andLibraryIdEqualTo(libraryid.intValue()).andStatusEqualTo(status);
        }else {
            example.or().andLibraryIdEqualTo(libraryid.intValue());
        }
        return (int) productsMapper.countByExample(example);
    }
    public List<Products> getAll(){
        ProductsExample example = new ProductsExample();
        return productsMapper.selectByExample(example);
    }

}
