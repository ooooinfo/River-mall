package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallRedPacaketsMapper;
import org.linlinjava.litemall.db.dao.SysWxConfigMapper;
import org.linlinjava.litemall.db.domain.LitemallRedPacakets;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsExample;
import org.linlinjava.litemall.db.domain.SysWxConfig;
import org.linlinjava.litemall.db.domain.SysWxConfigExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sound.midi.SysexMessage;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class SystemWxConfigService {
    @Resource
    private SysWxConfigMapper wxConfigMapper;

    public SysWxConfig getconfigByAppletNam(String appletname){
        SysWxConfigExample example = new SysWxConfigExample();
        example.or().andAppletNameEqualTo(appletname);
            return  wxConfigMapper.selectByExample(example).get(0);
    }
}
