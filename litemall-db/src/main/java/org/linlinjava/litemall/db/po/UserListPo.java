package org.linlinjava.litemall.db.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @Auther: wangly
 * @Date: 2019/5/28 0028 09:29
 */

@Data
@NoArgsConstructor
public class UserListPo implements Serializable {
  private static final long serialVersionUID = -28107234523498793L;

  private Long id;
  private String nickName;
  private String avatarUrl;
  private String userId;
  private int price;
  private int isSelf = 0;

}
