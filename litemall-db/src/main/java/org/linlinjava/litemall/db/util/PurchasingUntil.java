package org.linlinjava.litemall.db.util;

import org.linlinjava.litemall.db.domain.LitemallOrder;

import java.util.ArrayList;
import java.util.List;

/*
 * 采购流程：下单成功－》支付订单－》1688采购下单－》支付——》采购支付——》同步发货状态
 * 订单状态：
 * 801 未下采购单
 * 802 下单不成功（查看错误原因）
 * 803 下单成功待支付
 * 804 采购订单已支付
 * 805 供应商取消
 */
public class PurchasingUntil {

    public static final Integer STATUS_PURCHASING_WAIT = 801;
    public static final Integer STATUS_PURCHASING_FAIL = 802;
    public static final Integer STATUS_PURCHASING_WAIT2PAY = 803;
    public static final Integer STATUS_PURCHASING_PAYD = 804;
    public static final Integer STATUS_SUPPLIER_CANCLE = 805;


    public static String orderStatusText(LitemallOrder order) {
        int status = order.getPurchasingStatus().intValue();

        if (status == 801) {
            return "待下单";
        }

        if (status == 802) {
            return "下单失败";
        }

        if (status == 803) {
            return "待支付";
        }

        if (status == 804) {
            return "已支付";
        }

        if (status == 805) {
            return "供货商取消";
        }

        throw new IllegalStateException("purchasing不支持");
    }





}
