package org.linlinjava.litemall.db.po;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class OrderDetailPo implements Serializable {

	private static final long serialVersionUID = -1922533903886793220L;

	private String name;
	private String standard;
	private String logo;
	private Long price;
	private Long actualprice;
	private Long foodId;
	private Long foodDetailId;
	private Long orderId;
	private Long storeId;
	private int nums;

	private String deskId;
	private String ordernum;
	private Long inserttime;
	private Long paytime;
	// @JsonIgnore
	private String comment;
	// @JsonIgnore
	private Long sumprice;
	
	private String takeFoodCode;//取餐码
	
	private String storeName;
	
	private String address;
	
	private String userId;
	
	private int reviewed;//1已评价；0未评价
	
	private Long orderDetailId;
	private int prepay;

	public void setName(String name) {
		this.name = name;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public void setActualprice(Long actualprice) {
		this.actualprice = actualprice;
	}

	public void setFoodId(Long foodId) {
		this.foodId = foodId;
	}

	public void setFoodDetailId(Long foodDetailId) {
		this.foodDetailId = foodDetailId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public void setNums(int nums) {
		this.nums = nums;
	}

	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}

	public void setOrdernum(String ordernum) {
		this.ordernum = ordernum;
	}

	public void setInserttime(Long inserttime) {
		this.inserttime = inserttime;
	}

	public void setPaytime(Long paytime) {
		this.paytime = paytime;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setSumprice(Long sumprice) {
		this.sumprice = sumprice;
	}

	public void setTakeFoodCode(String takeFoodCode) {
		this.takeFoodCode = takeFoodCode;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setReviewed(int reviewed) {
		this.reviewed = reviewed;
	}

	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public void setPrepay(int prepay) {
		this.prepay = prepay;
	}

	public String getName() {
		return name;
	}

	public String getStandard() {
		return standard;
	}

	public String getLogo() {
		return logo;
	}

	public Long getPrice() {
		return price;
	}

	public Long getActualprice() {
		return actualprice;
	}

	public Long getFoodId() {
		return foodId;
	}

	public Long getFoodDetailId() {
		return foodDetailId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public int getNums() {
		return nums;
	}

	public String getDeskId() {
		return deskId;
	}

	public String getOrdernum() {
		return ordernum;
	}

	public Long getInserttime() {
		return inserttime;
	}

	public Long getPaytime() {
		return paytime;
	}

	public String getComment() {
		return comment;
	}

	public Long getSumprice() {
		return sumprice;
	}

	public String getTakeFoodCode() {
		return takeFoodCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public String getAddress() {
		return address;
	}

	public String getUserId() {
		return userId;
	}

	public int getReviewed() {
		return reviewed;
	}

	public Long getOrderDetailId() {
		return orderDetailId;
	}

	public int getPrepay() {
		return prepay;
	}
}
