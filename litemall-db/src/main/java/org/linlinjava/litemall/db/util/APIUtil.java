package org.linlinjava.litemall.db.util;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;

import org.linlinjava.litemall.db.domain.LitemallRedPacakets;
import org.linlinjava.litemall.db.po.UserParticipant;

public class APIUtil {
/**
 * 两个long型整数相除转String（剔除.00）
 * <p>Description: </p>
 * @param a
 * @param b
 * @return
 * @date 2019年5月9日
 */
	public static String doubleToString(Long a, Long b) {
		DecimalFormat df = new DecimalFormat("0.00");
		String str = df.format(a/(float)b);
		if(str.contains(".00")) {
			str=str.substring(0,str.length()-3);
		}
		return str;
	}
	

	public static List getRandomList(List list,int count){
     Collections.shuffle(list);
    List listNew = list.subList(0,count);
    return listNew;
  }

  /**
   * @Description: getRandom获取用户随机金额
   * @param: [userIdsCanDo, platformCutprice]
   * @return: java.util.List<com.tmxk.diesa.po.UserParticipant>
   * @auther: caiy
   * @date: 2019/7/23 0023 14:04
   */
  public static List<UserParticipant> getRandom(List<String> userIdsCanDo, LitemallRedPacakets platformCutprice, String user) throws Exception{
    int total = platformCutprice.getPrice();
    List<UserParticipant> userParticipants = new ArrayList<>();
    int sponserPrice = 0;

    Random ran = new Random();
    //###如果发起人可领取，则先计算出发起人可获得的金额
    if(platformCutprice.getSelfAvailable() ==1){
     // for(String userid:userIdsCanDo ){
    //  logger.info("userIdsCanDo 前"+userIdsCanDo);
      for (int l = userIdsCanDo.size() - 1; l >= 0; l--) {
        String userid = userIdsCanDo.get(l);
        //UserParticipant userParticipant = new UserParticipant();
        if(userid.equals(user)){
          int price = platformCutprice.getPrice()*platformCutprice.getSelfPercent()/100;
         // log.info("userid:"+userid+"========price:"+price+" platformCutprice.getPrice():"+platformCutprice.getPrice()+" platformCutprice.getSelfPercent:"+platformCutprice.getSelfPercent());
          sponserPrice = price;
          total -= price;
          //userIdsCanDo.remove(userid);
        }
      }
     /// log.info("userIdsCanDo 后"+userIdsCanDo);
    }
    //获取随机红包金额
    List<Integer> priceList = new ArrayList<>();
      priceList=APIUtil.random(userIdsCanDo.size(),total);
    //  log.info("userIdsCanDo.size():"+userIdsCanDo.size()+"  priceList.size():"+priceList.size());
      if(userIdsCanDo.size()==priceList.size()){
        for(int i = 0;i < userIdsCanDo.size(); i++ ){
          UserParticipant userParticipant = new UserParticipant();
          userParticipant.setUserId(userIdsCanDo.get(i));
          if(!userIdsCanDo.get(i).equals(user)) {
            userParticipant.setPrice(priceList.get(i));
          }else{
            userParticipant.setPrice(priceList.get(i)+sponserPrice);
          }
          userParticipants.add(userParticipant);
        }
      }
    return userParticipants;
  }

  public static  List<Integer> random(int n, int L){
    int bottom = 29;
    int bottoms = 29*n;
    L= L-bottoms;
    //log.info("L:"+L+" bottom"+bottom+" bottoms:"+29*n);
	  List<Integer> list = new ArrayList<>();
    Random rand = new Random();
    int temp = L;
    for(int i = n-2, j; i >=0; i--){
      j = rand.nextInt(temp-i-1) + 1;
      temp -= j;
      list.add(j+bottom);
    }
    list.add(temp+bottom);
    //System.out.println(list);
    Collections.shuffle(list);
    //System.out.println("======="+list);
  //  log.info("list:"+list);
    return  list;
  }
  public static double div(int d1, int d2) {// 进行除法运算
    BigDecimal b1 = new BigDecimal(d1);
    BigDecimal b2 = new BigDecimal(d2);
    double num = b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
    System.out.println(num);

    return num;
  }
  public static List removeEquals(List list1, List list2) {
    list1.removeAll(list2);
    System.out.println(list1);
    return list1;
  }
  public static void main(String[] args) throws ParseException {

    List _first=new ArrayList();

    _first.add("jack");
    //集合二
    List _second=new ArrayList();
    _second.add("jack");
    _second.add("happy");
    _second.add("sun");
    _second.add("good");
    APIUtil.removeEquals(_second,_first);
  }
}
