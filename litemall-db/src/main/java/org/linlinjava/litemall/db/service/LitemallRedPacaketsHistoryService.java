package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallRedPacaketsHistoryMapper;
import org.linlinjava.litemall.db.dao.LitemallRedPacaketsMapper;
import org.linlinjava.litemall.db.domain.LitemallRedPacakets;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsExample;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsHistory;
import org.linlinjava.litemall.db.domain.LitemallRedPacaketsHistoryExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
public class LitemallRedPacaketsHistoryService {
    @Resource
    private LitemallRedPacaketsHistoryMapper litemallRedPacaketsHistoryMapper;
    public  void updateRedpacaket(Long id ,int status){
        LitemallRedPacaketsHistory history = litemallRedPacaketsHistoryMapper.selectByPrimaryKey(id);
        history.setStatus(status);
        litemallRedPacaketsHistoryMapper.updateByPrimaryKey(history);
    }
    public void  updateredpacaletsHistory(Long id, int status,int parties,int totalReducation){
        LitemallRedPacaketsHistory history = litemallRedPacaketsHistoryMapper.selectByPrimaryKey(id);
        history.setStatus(status);
        history.setParties(parties);
        history.setTotalReducation(totalReducation);
        litemallRedPacaketsHistoryMapper.updateByPrimaryKey(history);
    }
    public LitemallRedPacaketsHistory saveAndreturn(LitemallRedPacaketsHistory history){
        litemallRedPacaketsHistoryMapper.insert(history);
        return history;
    }
    public void updateStatus(LitemallRedPacaketsHistory history){
        litemallRedPacaketsHistoryMapper.updateByPrimaryKeySelective(history);
    }
    public LitemallRedPacaketsHistory getCutRedpacaketById(long cutredpacaketsId){
       return   litemallRedPacaketsHistoryMapper.selectByPrimaryKey(cutredpacaketsId);
    }
    public List<LitemallRedPacaketsHistory> findAuctionByUserId(String openId,Long cutpriceId){
        LitemallRedPacaketsHistoryExample example = new LitemallRedPacaketsHistoryExample();
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        example.or().andUserIdEqualTo(openId).andCutpriceIdEqualTo(cutpriceId).andStatusIn(list);
        return litemallRedPacaketsHistoryMapper.selectByExample(example);
    }
    public List<LitemallRedPacaketsHistory> findAuctionByUserId(Long cutpriceId){
        LitemallRedPacaketsHistoryExample example  = new LitemallRedPacaketsHistoryExample();
        example.or().andCutpriceIdEqualTo(cutpriceId).andStatusEqualTo(2);
        return  litemallRedPacaketsHistoryMapper.selectByExample(example);
    }

    public void saveList(List<LitemallRedPacaketsHistory> sphs) {
        for(LitemallRedPacaketsHistory history:sphs){
            litemallRedPacaketsHistoryMapper.insert(history);
        }
    }
    public  List<LitemallRedPacaketsHistory> findRedPacaketsListByUserId(String userId){
            LitemallRedPacaketsHistoryExample example = new LitemallRedPacaketsHistoryExample();
            List<Integer> list = new ArrayList<>();
            list.add(0);
            list.add(1);
            list.add(2);
            //LocalDateTime.now().toEpochSecond(ZoneOffset.of("+8"));

            example.or().andUserIdEqualTo(userId).andStatusIn(list);
           // PageHelper.offsetPage(offset, limit);
            return litemallRedPacaketsHistoryMapper.selectByExample(example);
    }
    public  void  updatePlatformCutpriceHistoryfourParties(List<Long> hisIds){
        for(Long id:hisIds){
            LitemallRedPacaketsHistory history = new LitemallRedPacaketsHistory();
            history.setId(id);
            history.setStatus(3);
            litemallRedPacaketsHistoryMapper.updateByPrimaryKeySelective(history);
        }
    }
    public  List<LitemallRedPacaketsHistory>  getLimet20Userhistoy(){
        LitemallRedPacaketsHistoryExample example  = new LitemallRedPacaketsHistoryExample();
        example.or().andStatusEqualTo(0);
        PageHelper.offsetPage(1, 20);
        return litemallRedPacaketsHistoryMapper.selectByExample(example);
    }
    public  List<LitemallRedPacaketsHistory> findAuctionByUserIdAndStatus(String userId, Long cutprice_id, int status){
        LitemallRedPacaketsHistoryExample example = new LitemallRedPacaketsHistoryExample();
        example.or().andUserIdEqualTo(userId).andCutpriceIdEqualTo(cutprice_id).andStatusEqualTo(status);
        return litemallRedPacaketsHistoryMapper.selectByExample(example);
    }
}
