package org.linlinjava.litemall.db.po;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ProjectName: diesa_server
 * @Package: com.tmxk.diesa.po
 * @ClassName: UserParticipant
 * @Author: Administrator
 * @Description: ${description}
 * @Date: 2019/7/23 0023 13:17
 * @Version:
 */
@Data
@NoArgsConstructor
public class UserParticipant implements Serializable {
  private static final long serialVersionUID = -1992533903886793220L;

  private String userId;//
  private int price;//红包金额

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getUserId() {
    return userId;
  }

  public int getPrice() {
    return price;
  }
}
