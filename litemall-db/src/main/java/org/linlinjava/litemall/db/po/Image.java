/**  
 * <p>Title: Image.java</p>  
 * <p>Description: </p>    
 * @author caiy  
 * @date 2019年6月15日  
 */  
package org.linlinjava.litemall.db.po;

import lombok.Data;

/**  
 * <p>Title: Image</p>  
 * <p>Description: </p>  
 * @author caiy  
 * @date 2019年6月15日  
 */
@Data
public class Image {
	private String MediaId;
}
