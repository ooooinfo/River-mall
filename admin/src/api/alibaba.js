import request from '@/utils/request'

export function listCybUserGroup(query) {
  return request({
    url: '/alibaba/listCybUserGroup',
    method: 'get',
  })
}
export function listCybUserGroupFeed(query) {
  return request({
    url: '/alibaba/listCybUserGroupFeed',
    method: 'get',
    params: query
  })
}
export function productInfo(query) {
  return request({
    url: '/alibaba/productInfo',
    method: 'get',
    params: query
  })
}
export function getListByGroupId(query) {
  return request({
    url: '/alibaba/getListByGroupId',
    method: 'get',
    params: query
  })
}

export function batchSncyProducts(query) {
  return request({
    url: '/alibaba/batchSncyProducts',
    method: 'post',
    params: query
  })
}

export function productInfoByCateGoryId(query) {
  return request({
    url: '/alibaba/productInfoByCateGoryId',
    method: 'get',
    params: query
  })
}

export function deleteProduct(id) {
  return request({
    url: '/alibaba/deleteProduct',
    method: 'get',
    params: {id}
  })
}

