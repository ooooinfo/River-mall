/* eslint-disable */
var provinceData = [{
		"label": "Bali",
		"value": "11"
	},
	{
		"label": "Bangka Belitung",
		"value": "12"
	},
	{
		"label": "Banten",
		"value": "13"
	},
	{
		"label": "Bengkulu",
		"value": "14"
	},
	{
		"label": "DI Yogyakarta",
		"value": "15"
	},
	{
		"label": "DKI Jakarta",
		"value": "16"
	},
	{
		"label": "Gorontalo",
		"value": "17"
	},
	{
		"label": "Jambi",
		"value": "18"
	},
	{
		"label": "Jawa Barat",
		"value": "19"
	},
	{
		"label": "Jawa Tengah",
		"value": "20"
	},
	{
		"label": "Jawa Timur",
		"value": "21"
	},
	{
		"label": "Kalimantan Barat",
		"value": "22"
	}, {
		"label": "Kalimantan Selatan",
		"value": "23"
	},
	{
		"label": "Kalimantan Tengah",
		"value": "24"
	},
	{
		"label": "Kalimantan Timur",
		"value": "25"
	},
	{
		"label": "Kepulauan Riau",
		"value": "26"
	},
	{
		"label": "Maluku",
		"value": "27"
	},
	{
		"label": "Nanggroe Aceh Darussalam (NAD)",
		"value": "28"
	},
	{
		"label": "Nusa Tenggara Barat (NTB)",
		"value": "29"
	},
	{
		"label": "Nusa Tenggara Timur (NTT)",
		"value": "30"
	},
	{
		"label": "Papua",
		"value": "31"
	},
	{
		"label": "Papua Barat",
		"value": "32"
	},
	{
		"label": "Riau",
		"value": "33"
	},
	{
		"label": "Sulawesi Tengah",
		"value": "34"
	},
	{
		"label": "Sulawesi Tenggara",
		"value": "35"
	},
	{
		"label": "Sulawesi Utara",
		"value": "36"
	},
	{
		"label": "Sumatera Barat",
		"value": "37"
	},
	{
		"label": "Sumatera Selatan",
		"value": "38"
	},
	{
		"label": "Sumatera Utara",
		"value": "39"
	}
]
export default provinceData;
